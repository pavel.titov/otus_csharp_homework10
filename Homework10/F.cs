﻿namespace Homework10
{
    class F
    {
        int i1, i2, i3, i4, i5;

        public static F Get() => new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };

        public override string ToString()
        {
            return string.Join(';', i1, i2, i3, i4, i5);
        }
    }
}
