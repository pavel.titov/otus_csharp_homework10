﻿using System;
using System.Diagnostics;
using System.Text.Json;

namespace Homework10
{
    class Program
    {
        static void Main(string[] args)
        {
            var f = F.Get();

            string csv = MyCsvSerializer.Serialize(f);
            Console.WriteLine($"Serialized:");
            Console.WriteLine(csv);

            Console.WriteLine();

            var deserialized = MyCsvSerializer.Deserialize<F>(csv);
            Console.WriteLine($"Deserialized:");
            Console.WriteLine(deserialized);

            Console.WriteLine();


            string json = JsonSerializer.Serialize(f);

            long timeJsonSerialize = Time(() =>
            {
                JsonSerializer.Serialize(json);
            });

            long timeJsonDeserialize = Time(() =>
            {
                JsonSerializer.Deserialize<F>(json);
            });

            long timeMyCsvSerialize = Time(() =>
            {
                MyCsvSerializer.Serialize(f);
            });

            long timeMyCsvDeserialize = Time(() =>
            {
                MyCsvSerializer.Deserialize<F>(csv);
            });

            Console.WriteLine($"timeJsonSerialize: {timeJsonSerialize}");
            Console.WriteLine($"timeJsonDeserialize: {timeJsonDeserialize}");
            Console.WriteLine($"timeMyCsvSerialize: {timeMyCsvSerialize}");
            Console.WriteLine($"timeMyCsvDeserialize: {timeMyCsvDeserialize}");

            Console.ReadKey();
        }

        static long Time(Action a)
        {
            var sw = Stopwatch.StartNew();
            for (int i = 0; i < 100_000; ++i)
            {
                a();
            }
            return sw.ElapsedMilliseconds;
        }
    }
}
