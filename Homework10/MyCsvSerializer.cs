﻿using System;
using System.Text;
using System.Reflection;

namespace Homework10
{
    class MyCsvSerializer
    {
        private static readonly char delimiter = ';';

        public static string Serialize<T>(T instance)
        {
            Type type = instance.GetType();
            var sb = new StringBuilder();

            var fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (var field in fields)
            {
                sb.AppendLine(string.Join(delimiter, field.Name, field.GetValue(instance)));
            }

            return sb.ToString().TrimEnd();
        }

        public static T Deserialize<T>(string source)
        {
            Type type = typeof(T);
            var instance = Activator.CreateInstance(type);

            var stringFields = source.Split(Environment.NewLine);
            foreach (var s in stringFields)
            {
                var nameAndValue = s.Split(delimiter);
                string name = nameAndValue[0];
                string stringValue = nameAndValue[1];
                object value = GetTypedBoxed(stringValue);

                var field = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Instance);
                field.SetValue(instance, value);
            }

            return (T)instance;
        }

        private static object GetTypedBoxed(string value)
        {
            return value switch
            {
                var x when int.TryParse(x, out int res) => res,
                // other types
                _ => value
            };
        }
    }
}
